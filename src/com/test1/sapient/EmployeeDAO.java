package com.test1.sapient;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class EmployeeDAO {
	String sourcefile = "C:\\Users\\sacrana1\\Desktop\\tests\\src\\com\\test1\\sapient\\test.csv";
	
	List <EmployeeBean> EmployeeDetailsList = new ArrayList<EmployeeBean>();

	private BufferedReader br;
	
	//Read data from csv file
	public List<EmployeeBean> readData () throws IOException{
		
		try {
		FileReader filereader = new FileReader(sourcefile);
		String line;
		EmployeeBean employee = null;
		br = new BufferedReader(filereader);
		while((line=br.readLine())!= null) {
			String[] s1 = line.split(",");
			employee = new EmployeeBean(Integer.parseInt(s1[0]),s1[1],Integer.parseInt(s1[2]));
			EmployeeDetailsList.add(employee);
		}
		
		br.close();
		}
		catch (IOException e){
			e.printStackTrace();
			
		}
		catch (Exception e) {
			
		}
		catch (Throwable e) {
			
		}
		
		return EmployeeDetailsList;
		
	}
	
	
	//Get average salary
	
	public double getToSal(List <EmployeeBean> EmployeeDetailslist) throws Exception {
		double totalSalary;
		totalSalary = 0.0;
		totalSalary = EmployeeDetailslist.stream().filter(employee->employee.getSalary()>=0).mapToDouble(employee->employee.getSalary()).sum();
		return totalSalary;
		
	}
	
	
	//Get Count of number of Employees drawing the current salary
	public int getCount(List <EmployeeBean> Employeedetailslist , int salary) {
		int countOfEmployess;
		countOfEmployess = 0;
		try {
		countOfEmployess = (int) Employeedetailslist.stream().filter(emp->emp.getSalary()== salary).count();
		}
		catch (Exception e) {
			System.out.println("Employee list not found or salary is not numeric");
		}
	return countOfEmployess;
	}
	
	//Get Employee Details for the given Id
	
	public EmployeeBean getEmployee(List <EmployeeBean>list, int id) {
		List<EmployeeBean> templist = null;
	
		try {
			templist = list.stream().filter(emp->emp.getId()==id).collect(Collectors.toList());
		}
		catch (Exception e) {
			System.out.println("No employee found with this employee ID");
		}
		return templist.get(0);
	}
}
