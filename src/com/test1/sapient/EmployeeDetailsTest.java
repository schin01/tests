package com.test1.sapient;

import static org.junit.jupiter.api.Assertions.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

class EmployeeDetailsTest {
	EmployeeDAO  empobj =  new EmployeeDAO();
	List <EmployeeBean> list = new ArrayList<EmployeeBean>();
	//Test to check import of file
	@Test
	void test() {
		try {
			list= empobj.readData();
			}
		catch(Exception e) {
			System.out.println(e);}	
	}
	
	//test to check the total salary count
	@Test
	void test1(){
		try {
			list= empobj.readData();
			assertEquals(empobj.getToSal(list),600.0);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	//test to check the number of employeess having the said salary
	@Test
	void test2() {
		try {
			list= empobj.readData();
			int salary;
			salary= 10;
			assertEquals(empobj.getCount(list,salary),2);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	//test to fetch employee details when id is given
	@Test
	void test3() {
		int id = 3;
		try {
			list= empobj.readData();
			assertEquals(empobj.getEmployee(list, id).equals(new EmployeeBean(3,"C",30)),true);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}
